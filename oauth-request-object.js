import jwt from 'jsonwebtoken';
import uuidv1 from 'uuid/v1';
const appCallback = 'https://yourapplication.com/auth/callback';
const getExp = (minutes = 60) =>
    Math.floor (Date.now () / 1000) + (minutes * 60);
const getClaims = (
    type, // accounts | payments
    consentId
) => ({
    userinfo: {
        openbanking_intent_id: {
            value: `urn:obiebank:${type}:${consentId}`,
            essential: true
        }
    },
    id_token: {
        openbanking_intent_id: {
            value: `urn:obiebank:${type}:${consentId}`,
            essential: true
        },
        acr: {
            essential: true,
            values: [
                'urn:openbanking:psd2:sca',
                'urn:openbanking:psd2:ca'
            ]
        }
    }
});
const tokenSetup = (
    type,
    consentId
) => ({
    algorithm: 'RS256',
    aud: 'https://auth.obiebank.banfico.com',
    responseType: 'code id_token',
    scope: `openid profile email ${type}`,
    redirectUri: appCallback,
    claims: getClaims (type, consentId),
    state: uuidv1 (),
    nonce: uuidv1 ()
})
const createRequestObj = ({
                              privateKey,
                              consentId,
                              clientId,
                              type
                          }) => {
    try {
        const {
            aud,
            claims,
            scope,
            state,
            nonce,
            algorithm,
            responseType,
            redirectUri
        } = tokenSetup (type, consentId);
        const header = {};
        const payload = {};
        payload.aud = aud;
        payload.iss = clientId;
        payload.client_id = clientId;
        payload.redirect_uri = redirectUri;
        payload.scope = scope;
        payload.state = state;
        payload.nonce = nonce;
        payload.exp = getExp ();
        payload.response_type = responseType;
        payload.claims = claims;
        return jwt.sign (payload, privateKey, {algorithm, header});
    } catch (e) {
        console.error ('* jwt sign error:', e.message);
        return 'JWT sign error: private key is missing or invalid';
    }
}